class CreateSells < ActiveRecord::Migration[5.1]
  def change
    create_table :sells do |t|
      t.text :description
      t.string :sns

      t.timestamps
    end
  end
end
