class CreateCollects < ActiveRecord::Migration[5.1]
  def change
    create_table :collects do |t|
      t.text :description
      t.string :sns

      t.timestamps
    end
  end
end
